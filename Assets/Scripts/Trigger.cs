﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    private Animator animator;
    private AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        audio = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }


    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Collider"))
        {
            animator.SetTrigger("Fire");
            audio.Play();
        }
    }
}
